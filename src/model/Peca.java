package model;

import java.awt.Point;
import javax.swing.text.Position;
import java.util.ArrayList;

public abstract class Peca{
	private String imagePath;
	private String typeOfObject;
	private ArrayList<Position> move;
	
	public Peca(String imagePath) {
		this.imagePath = imagePath;
	}

	public Peca(String imagePath, String typeOfObject) {
		this.imagePath = imagePath;
		this.typeOfObject = typeOfObject;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getTypeOfObject() {
		return typeOfObject;
	}

	public void setTypeOfObject(String typeOfObject) {
		this.typeOfObject = typeOfObject;
	}

	public ArrayList<Position> getMove() {
		return move;
	}
	public void setMove(ArrayList<Position> move) {
		this.move = move;
	}

	public ArrayList<Point> getMove(int x, int y) {
		return null;
	}
	public ArrayList<Point> getMoveW(int x, int y) {
		return null;
	}
	public ArrayList<Point> getMoveB(int x, int y) {
		return null;
}
	
}